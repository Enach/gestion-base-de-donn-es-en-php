<?php
require('inc/config.php');
?>
    <html>
    <head>
        <title>Bienvenue sur cette page</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="css/bootstrap-index.css">
    </head>
    <body>
    <div class="container">
        <div class="page-header">
            <h1>Bienvenue sur la page de ce site</h1>
        </div>
        <a href="inc/add.php">
            <button class="btn btn-info">Ajouter un membre</button>
        </a>

        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Matricule</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Niveau</th>
                <th>Licence</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            try {
                $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $stmt = $conn->prepare("SELECT Adherents.id, Adherents.Nom, Adherents.Prenom, Niveau.libelle, Licence.libelleType FROM (Adherents, Licence, Niveau) WHERE Adherents.Niveau = Niveau.id AND Adherents.Licence = Licence.numType ORDER BY id");
                $stmt->execute();
                // set the resulting array to associative
                $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $resultat = $stmt->fetchAll();

                foreach ($resultat as $val) { ?>
                    <tr>
                        <td>
                            <?php echo $val['id']; ?>
                        </td>
                        <td>
                            <?php echo utf8_encode($val['Nom']); ?>
                        </td>
                        <td>
                            <?php echo utf8_encode($val['Prenom']); ?>
                        </td>
                        <td>
                            <?php echo utf8_encode($val['libelle']); ?>
                        </td>
                        <td>
                            <?php echo utf8_encode($val['libelleType']); ?>
                        </td>
                        <td>
                            <p>
                                <a href="inc/edit.php?id=<?php echo $val['id'] ?>">
                                    <button class="btn btn-warning btn-sm">Editer</button>
                                </a>&nbsp;
                                <a href="inc/action.php?id=<?php echo $val['id'] ?>&amp;type=delete">
                                    <button class="btn btn-danger btn-sm">Supprimer</button>
                                </a>
                            </p>
                        </td>
                    </tr>
                <?php }
            } catch
            (PDOException $e) {
                echo "Error: " . $e->getMessage();
            }
            $conn = null;
            ?>
            </tbody>
        </table>
    </div>
    </body>
    </html>
<?
$bdd = new PDO('mysql:host=localhost:8889;dbname=BadmintonClub', 'root', '');

if (isset($_POST['choix'])) {

    $ville = $_POST['ville'];
    $adresse = $_POST['adresse'];
    $cp = $_POST['cp'];
    $choix = $_POST['type'];
    $nom = $_POST ['nom'];
    $prenom = $_POST['prenom'];
    $niveau = $_POST['niveau'];

    echo $ville;
    echo $adresse;
    echo $cp;
    echo $choix;
    echo $nom;
    echo $prenom;
    echo $niveau;

    $req = $bdd->prepare('INSERT INTO ADHERENT(nomAdh, prenomAdh,adresseAdh, villeAdh, cpAdh, niveauAdh) VALUES(:nomAdh,:prenomAdh, :adresseAdh, :villeAdh, :cpAdh, :niveauAdh)');
    $req->execute(array(
        'nomAdh' => $nom,
        'prenomAdh' => $prenom,
        'adresseAdh' => $adresse,
        'villeAdh' => $ville,
        'cpAdh' => $cp,
        'niveauAdh' => $niveau
    ));
}
?>