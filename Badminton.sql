-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 27 Janvier 2015 à 14:12
-- Version du serveur :  5.5.40-0+wheezy1
-- Version de PHP :  5.4.35-0+deb7u2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `Badminton`
--

-- --------------------------------------------------------

--
-- Structure de la table `Adherents`
--

CREATE TABLE IF NOT EXISTS `Adherents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) NOT NULL,
  `Prenom` varchar(255) NOT NULL,
  `Niveau` int(11) NOT NULL,
  `Licence` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Niveau` (`Niveau`),
  KEY `Licence` (`Licence`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `Adherents`
--

INSERT INTO `Adherents` (`id`, `Nom`, `Prenom`, `Niveau`, `Licence`) VALUES
(1, 'Duppont', 'Michel', 1, 1),
(2, 'Dapart', 'Géraldine', 4, 2),
(4, 'Kurk', 'James', 3, 2),
(6, 'shrek', 'Mulet', 1, 1),
(8, 'Jean', 'Bon', 3, 4),
(9, 'Eric', 'roi', 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Licence`
--

CREATE TABLE IF NOT EXISTS `Licence` (
  `numType` int(11) NOT NULL AUTO_INCREMENT,
  `libelleType` varchar(255) NOT NULL,
  `montantLicence` int(11) NOT NULL,
  PRIMARY KEY (`numType`),
  KEY `libelleType` (`libelleType`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `Licence`
--

INSERT INTO `Licence` (`numType`, `libelleType`, `montantLicence`) VALUES
(1, 'Salarié', 27),
(2, 'Etudiant', 20),
(3, 'Retraité', 23),
(4, 'Patron', 30),
(5, 'Chomeur', 25);

-- --------------------------------------------------------

--
-- Structure de la table `Niveau`
--

CREATE TABLE IF NOT EXISTS `Niveau` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `libelle` (`libelle`),
  KEY `libelle_2` (`libelle`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `Niveau`
--

INSERT INTO `Niveau` (`id`, `libelle`) VALUES
(5, 'Baby'),
(2, 'Confirmé'),
(1, 'Débutant'),
(3, 'Expert'),
(4, 'Novice');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Adherents`
--
ALTER TABLE `Adherents`
  ADD CONSTRAINT `Adherent_Licence` FOREIGN KEY (`Licence`) REFERENCES `Licence` (`numType`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Adherent_Niveau` FOREIGN KEY (`Niveau`) REFERENCES `Niveau` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
