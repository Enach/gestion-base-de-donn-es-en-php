<?php
require('config.php');
?>
            <table class = "table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Matricule</th>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Niveau</th>
                        <th>Licence</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    try {
                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $stmt = $conn->prepare("SELECT Adherents.id, Adherents.Nom, Adherents.Prenom, Niveau.libelle, Licence.libelleType FROM (Adherents, Licence, Niveau) WHERE Adherents.Niveau = Niveau.id AND Adherents.Licence = Licence.numType ORDER BY id");
                        $stmt->execute();
                        // set the resulting array to associative
                        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
                        $resultat = $stmt->fetchAll();

                        foreach ($resultat as $val) { ?>
                            <tr>
                                <td>
                                    <?php echo $val['id']; ?>
                                </td>
                                <td>
                                    <?php echo utf8_encode($val['Nom']); ?>
                                </td>
                                <td>
                                    <?php echo utf8_encode($val['Prenom']); ?>
                                </td>
                                <td>
                                    <?php echo utf8_encode($val['libelle']); ?>
                                </td>
                                <td>
                                    <?php echo utf8_encode($val['libelleType']); ?>
                                </td>
                                <td>
                                    <p>
                                        <a href = "edit.php?id=<?php echo $val['id'] ?>">
                                            <button class = "btn btn-warning btn-sm">Editer</button>
                                        </a>&nbsp;
                                        <a href = "action.php?id=<?php echo $val['id'] ?>&amp;type=delete">
                                            <button class = "btn btn-danger btn-sm">Supprimer</button>
                                        </a>
                                    </p>
                                </td>
                            </tr>
                        <?php }
                    } catch
                    (PDOException $e) {
                        echo "Error: " . $e->getMessage();
                    }
                    $conn = null;
                    ?>
                </tbody>
            </table>
