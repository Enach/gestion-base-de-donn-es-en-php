<?php
require('config.php');
?>
<html>
    <head>
        <title>Editer un membre</title>
        <meta charset = "utf-8"/>
        <link rel = "stylesheet" type = "text/css" href = "../css/bootstrap-index.css">
    </head>
    <body>
        <div class = "container">
            <div class = "page-header">
                <h1>Editer un membre</h1>
            </div>
            <div class = "bs-docs-section clearfix">
                <div class = "row">
                    <div class = "well bs-component">
                        <?php
                        $id = $_GET['id'];

                        try {
                            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                            $stmt = $conn->prepare("SELECT * FROM Adherents WHERE id = '$id'");
                            $stmt->execute();
                            $membre = $stmt->fetch();
                        } catch
                        (PDOException $e) {
                            echo "Error: " . $e->getMessage();
                        }
                        $nom = $membre['Nom'];
                        $prenom = $membre['Prenom'];
                        $niveauAdherent = $membre['Niveau'];
                        $licenceAdherent = $membre['Licence'];

                        echo $licence;

                        ?>
                        <form class = "form-horizontal" method = "POST" action = "action.php">
                            <fieldset>
                                <input style = "display: none" type = "text" value = "edit" name = "type" id = "type"
                                       title = "Type">
                                <input style = "display: none" value = "<?php echo $id; ?>" name = "id" id = "id">

                                <div class = "row">
                                    <div class = "col-md-6">
                                        <div class = "form-group">
                                            <label for = "name">Nom</label>

                                            <div class = "col-lg-10">
                                                <input class = "form-control" type = "text" name = "name" id = "name"
                                                       value = "<?php echo utf8_encode($nom) ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "col-md-6">
                                        <div class = "form-group">
                                            <label for = "firstname">Prénom</label>

                                            <div class = "col-lg-10">
                                                <input class = "form-control" type = "text" name = "firstname"
                                                       id = "firstname"
                                                       value = "<?php echo utf8_encode($prenom) ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "row">
                                    <div class = "col-md-6">
                                        <div class = "form-group">
                                            <label for = "level">Niveau</label>

                                            <div class = "col-lg-10">
                                                <select multiple class = "form-control" name = "level" id = "level">
                                                    <?php
                                                    try {

                                                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                                        $stmt = $conn->prepare("SELECT id, libelle FROM Niveau WHERE id = $niveauAdherent");
                                                        $stmt->execute();
                                                        $niveau = $stmt->fetch();
                                                        ?>
                                                        <option
                                                            value = "<?php echo $niveau['id'] ?>"><?php echo utf8_encode($niveau['libelle']) ?></option>
                                                    <?php
                                                    } catch
                                                    (PDOException $e) {
                                                        echo "Error: " . $e->getMessage();
                                                    }

                                                    try {

                                                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                                        $stmt = $conn->prepare("SELECT id, libelle FROM Niveau WHERE id != $niveauAdherent ORDER BY libelle");
                                                        $stmt->execute();
                                                        // set the resulting array to associative
                                                        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
                                                        $niveaux = $stmt->fetchAll();


                                                        foreach ($niveaux as $val) {
                                                            echo "<option value='";
                                                            echo $val['id'];
                                                            echo "'>";
                                                            echo utf8_encode($val['libelle']);
                                                            echo "</option>";
                                                        }
                                                    } catch
                                                    (PDOException $e) {
                                                        echo "Error: " . $e->getMessage();
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "col-md-6">
                                        <div class = "form-group">
                                            <label for = "licence">Licence</label>

                                            <div class = "col-lg-10">
                                                <select multiple class = "form-control" name = "licence" id = "licence">
                                                    <?php

                                                    try {

                                                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                                        $stmt = $conn->prepare("SELECT numType, libelleType FROM Licence WHERE numType = $licenceAdherent");
                                                        $stmt->execute();
                                                        // set the resulting array to associative
                                                        $licence = $stmt->fetch();
                                                        ?>
                                                        <option
                                                            value = "<?php echo $licence['numType'] ?>"><?php echo utf8_encode($licence['libelleType']) ?></option>
                                                    <?php
                                                    } catch
                                                    (PDOException $e) {
                                                        echo "Error: " . $e->getMessage();
                                                    }
                                                    try {

                                                        $stmt = $conn->prepare("SELECT numType, libelleType FROM Licence WHERE numType != $licenceAdherent ORDER BY libelleType");
                                                        $stmt->execute();
                                                        // set the resulting array to associative
                                                        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
                                                        $licences = $stmt->fetchAll();

                                                        foreach ($licences as $val) {
                                                            echo "<option value='";
                                                            echo $val['numType'];
                                                            echo "'>";
                                                            echo utf8_encode($val['libelleType']);
                                                            echo "</option>";
                                                        }
                                                    } catch
                                                    (PDOException $e) {
                                                        echo "Error: " . $e->getMessage();
                                                    }
                                                    $conn = null;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "col-md-4 pull-right">
                                    <div class = "form-group">
                                        <div class = "col-lg-10 col-lg-offset-2">
                                            <a href = "http://62.220.62.210/romain/bts">
                                                <button class = "text-center btn btn-primary">Annuler</button>
                                            </a>
                                            <button type = "submit" class = "text-center btn btn-success">
                                                Editer
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <?php include('Tableau.php'); ?>
        </div>